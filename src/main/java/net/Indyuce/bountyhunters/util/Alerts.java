package net.Indyuce.bountyhunters.util;

import net.Indyuce.bountyhunters.api.Bounty;
import net.Indyuce.bountyhunters.api.event.BountyCreateEvent;
import net.Indyuce.bountyhunters.api.language.Message;
import net.Indyuce.bountyhunters.api.language.PlayerMessage;
import net.Indyuce.bountyhunters.api.player.PlayerData;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class Alerts {

    public static void bountyRewardIncrease(@NotNull Bounty bounty) {
        final PlayerMessage toOnline = Message.BOUNTY_CHANGE.format(
                "player", bounty.getTarget().getName(),
                "reward", UtilityMethods.format(bounty.getReward()));

        for (Player online : Bukkit.getOnlinePlayers())
            (bounty.hasTarget(online) ? Message.BOUNTY_INCREASE_SELF.format(
                    "reward", UtilityMethods.format(bounty.getReward()))
                    : toOnline).send(online);
    }

    public static void bountyUnregistered(@NotNull Bounty bounty, double oldReward) {
        final PlayerMessage toOnline = Message.BOUNTY_EXPIRED.format(
                "target", bounty.getTarget().getName(),
                "bounty", UtilityMethods.format(oldReward));

        for (Player online : Bukkit.getOnlinePlayers())
            (bounty.hasTarget(online) ? Message.BOUNTY_EXPIRED_SELF.format(
                    "bounty", UtilityMethods.format(oldReward))
                    : toOnline).send(online);
    }

    public static void bountyRewardDecrease(@NotNull Bounty bounty, @Nullable Player player, double amount) {
        final double newReward = bounty.getReward();
        final PlayerMessage toOnline = Message.BOUNTY_DECREASED.format(
                "target", bounty.getTarget().getName(),
                "old", UtilityMethods.format(newReward + amount),
                "new", UtilityMethods.format(newReward),
                "player", player == null ? "Server" : player.getName(),
                "amount", UtilityMethods.format(amount));

        for (Player online : Bukkit.getOnlinePlayers())
            (bounty.hasTarget(online) ? Message.BOUNTY_DECREASED_SELF.format(
                    "old", UtilityMethods.format(newReward + amount),
                    "new", UtilityMethods.format(newReward),
                    "player", player == null ? "Server" : player.getName(),
                    "amount", UtilityMethods.format(amount))
                    : toOnline).send(online);
    }

    public static void bountyClaimed(@NotNull Bounty bounty, @NotNull Player claimer) {
        final String reward = UtilityMethods.format(bounty.getReward());
        final PlayerData playerData = PlayerData.get(claimer);
        final String title = playerData.hasTitle() ? ChatColor.LIGHT_PURPLE + "[" + playerData.getTitle().format() + ChatColor.LIGHT_PURPLE + "] " : "";
        final PlayerMessage toOnline = Message.BOUNTY_CLAIMED.format(
                "reward", UtilityMethods.format(bounty.getReward()),
                "killer", title + claimer.getName(),
                "target", bounty.getTarget().getName());

        for (Player online : Bukkit.getOnlinePlayers())
            (online.equals(claimer) ? Message.BOUNTY_CLAIMED_BY_YOU.format(
                    "target", bounty.getTarget().getName(),
                    "reward", reward)
                    : (bounty.getTarget().equals(online) ? Message.BOUNTY_CLAIMED_SELF.format(
                    "killer", title + claimer.getName(),
                    "reward", reward)
                    : toOnline)).send(online);
    }

    public static void bountyCreate(@NotNull Bounty bounty, @NotNull BountyCreateEvent.BountyCause cause) {
        final String reward = UtilityMethods.format(bounty.getReward());
        final PlayerMessage toOnline = (cause == BountyCreateEvent.BountyCause.PLAYER ?
                Message.NEW_BOUNTY_ON_PLAYER : cause == BountyCreateEvent.BountyCause.AUTO_BOUNTY ?
                Message.NEW_BOUNTY_ON_PLAYER_ILLEGAL : Message.NEW_BOUNTY_ON_PLAYER_UNDEFINED)
                .format(
                        "creator", bounty.hasCreator() ? bounty.getCreator().getName() : "Server",
                        "target", bounty.getTarget().getName(),
                        "reward", reward);

        for (Player player : Bukkit.getOnlinePlayers()) {

            // To bounty target
            if (bounty.hasTarget(player))
                (cause == BountyCreateEvent.BountyCause.PLAYER ?
                        Message.NEW_BOUNTY_ON_YOU : cause == BountyCreateEvent.BountyCause.AUTO_BOUNTY
                        ? Message.NEW_BOUNTY_ON_YOU_ILLEGAL : Message.NEW_BOUNTY_ON_YOU_UNDEFINED)
                        .format(
                                "creator", bounty.hasCreator() ? bounty.getCreator().getName() : "null", "target",
                                bounty.getTarget().getName(), "reward", reward)
                        .send(player);

                // To bounty creator
            else if (bounty.hasCreator(player))
                Message.BOUNTY_CREATED.format(
                        "creator", bounty.hasCreator() ? bounty.getCreator().getName() : "null",
                        "target",
                        bounty.getTarget().getName(),
                        "reward", reward).send(player);

                // Any other player
            else
                toOnline.send(player);
        }
    }
}
