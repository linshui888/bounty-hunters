package net.Indyuce.bountyhunters.api.event;

import net.Indyuce.bountyhunters.api.Bounty;
import net.Indyuce.bountyhunters.util.Alerts;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nullable;

public class BountyIncreaseEvent extends BountyEvent {
    private final CommandSender source;
    private final BountyChangeCause cause;

    private double added;

    private static final HandlerList HANDLERS = new HandlerList();

    /**
     * Called when the total bounty reward changes either when the auto bounty
     * increases the reward since the player killed another player illegaly; or
     * when a player manually increases an existing bounty using /addbounty
     *
     * @param bounty Bounty being increased
     * @param source Either the server console, a player, or a command block
     * @param added  Amount of cash being added in
     * @param cause  Reason why the bounty reward changes
     */
    public BountyIncreaseEvent(Bounty bounty, CommandSender source, double added, BountyChangeCause cause) {
        super(bounty);

        this.source = source;
        this.cause = cause;
        this.added = added;
    }

    public double getAdded() {
        return added;
    }

    public void setAdded(double added) {
        this.added = added;
    }

    @NotNull
    public BountyChangeCause getCause() {
        return cause;
    }

    @Nullable
    public Player getPlayer() {
        return source instanceof Player ? (Player) source : null;
    }

    public boolean hasPlayer() {
        return source instanceof Player;
    }

    @NotNull
    public CommandSender getSource() {
        return source;
    }

    @Deprecated
    public void sendAllert() {
        Alerts.bountyRewardIncrease(getBounty());
    }

    @NotNull
    public HandlerList getHandlers() {
        return HANDLERS;
    }

    public static HandlerList getHandlerList() {
        return HANDLERS;
    }

    public static enum BountyChangeCause {

        /**
         * When a player adds money to a bounty using /bounty
         */
        PLAYER,

        /**
         * When the server (or a command block) adds money to a bounty using /bounty
         */
        CONSOLE,

        /**
         * When the auto bounty increases a player's bounty after a player
         * has killed somebody else illegally
         */
        AUTO_BOUNTY,

        /**
         * Extra cause which can be used by other plugins/addons
         */
        PLUGIN;
    }
}
