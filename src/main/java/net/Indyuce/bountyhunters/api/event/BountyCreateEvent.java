package net.Indyuce.bountyhunters.api.event;

import net.Indyuce.bountyhunters.api.Bounty;
import net.Indyuce.bountyhunters.util.Alerts;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class BountyCreateEvent extends BountyEvent {
    private final BountyCause cause;
    private final CommandSender source;

    private static final HandlerList handlers = new HandlerList();

    /**
     * This event is called whenever a player sets a bounty onto another player,
     * or when the auto-bounty automatically sets a new bounty on a player since
     * they killed someone illegally
     *
     * @param bounty Bounty being created
     * @param source Either the server (auto-bounties), a player, or even a command block
     * @param cause  The reason
     */
    public BountyCreateEvent(@NotNull Bounty bounty, @NotNull CommandSender source, @NotNull BountyCause cause) {
        super(bounty);

        this.cause = cause;
        this.source = source;
    }

    @NotNull
    public BountyCause getCause() {
        return cause;
    }

    @Nullable
    public Player getCreator() {
        return source instanceof Player ? (Player) source : null;
    }

    @NotNull
    public CommandSender getSource() {
        return source;
    }

    public boolean hasCreator() {
        return source instanceof Player;
    }

    @Deprecated
    public void sendAllert() {
        Alerts.bountyCreate(getBounty(), cause);
    }

    @NotNull
    public HandlerList getHandlers() {
        return handlers;
    }

    @NotNull
    public static HandlerList getHandlerList() {
        return handlers;
    }

    public static enum BountyCause {

        /**
         * When a player sets a bounty onto another player's head
         */
        PLAYER,

        /**
         * When a non-player entity (console/command block) sets a bounty on a
         * player's head
         */
        CONSOLE,

        /**
         * When the auto bounty sets a bounty on a player since he killed
         * someone illegaly (illegaly = the player did not have any bounty on
         * them, which makes it an illegal kill)
         */
        AUTO_BOUNTY,

        /**
         * Extra bounty cause that is not used in the vanilla BountyHunters but
         * that can be used by other plugins
         */
        PLUGIN;
    }
}
