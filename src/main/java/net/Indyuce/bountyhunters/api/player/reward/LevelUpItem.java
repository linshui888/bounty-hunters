package net.Indyuce.bountyhunters.api.player.reward;

import net.Indyuce.bountyhunters.api.AltChar;
import net.Indyuce.bountyhunters.api.player.PlayerData;
import org.apache.commons.lang3.Validate;

import java.util.Objects;

public abstract class LevelUpItem {
    private final String id, format;
    private final int unlock;

    /**
     * Public constructor for items acquired when leveling up
     *
     * @param id     Item ID, has to be different from all the others
     * @param format Title format or claim quote for bounty animations
     * @param unlock Level at which item is unlocked
     */
    public LevelUpItem(String id, String format, int unlock) {
        Validate.notNull(id, "Item ID must not be null");
        Validate.notNull(format, "Item format must not be null");

        this.id = id;
        this.format = format;
        this.unlock = unlock;
    }

    public String getId() {
        return id;
    }

    public int getUnlockLevel() {
        return unlock;
    }

    public boolean hasUnlocked(PlayerData data) {
        return data.getLevel() > unlock;
    }

    public String format() {
        return AltChar.apply(format);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LevelUpItem that = (LevelUpItem) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}