<p align="center">
  <img src="https://phoenixdevt.fr/img/plugin/header_list/bountyhunters.png" alt="BountyHunters"/>
</p>
<h2 align="center">BountyHunters Premium</h2>
<p align="center">
<a href="https://www.spigotmc.org/resources/72142/">Premium Version</a> | 
<a href="https://www.spigotmc.org/resources/40610/">Lite Version</a> | 
<a href="https://bstats.org/plugin/bukkit/BountyHunters">Metrics</a> | 
<a href="https://gitlab.com/phoenix-dvpmt/bounty-hunters/-/wikis/home">Documentation</a> | 
<a href="https://gitlab.com/phoenix-dvpmt/bounty-hunters/-/issues">Issue Tracker</a>
</p>

---

BountyHunters is an open source project and aims at providing an all-in-one bounty solution for spigot servers. If you would like to contribute to the project, you can buy the paid version over Spigot for 9€ however a free version is available over Spigot as well. This repository corresponds to the latest **premium build**, that means you are also free to edit and compile the plugin on your end.

### Bugs & Suggestions
**Please open a ticket if you need help with anything related to the plugin, I do not use the Spigot plugin discussion.** The wiki presents the main plugin features as well as the main API methods and classes, don't hesitate to have a look before opening an issue to make sure it does not have the answer you're looking for.

Bug reports and feature suggestions keep the project alive too!